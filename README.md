# Plugin Date de connexion pour Spip

Spip trace nativement la date de dernière connexion des auteurs dans le champ `en_ligne`, mais il y a 2 limites (cf. spip/spip#3480) :

* cela concerne uniquement les connexions à l'espace privé
* le champ est remis à zéro à chaque déconnexion

Ce plugin complète ce fonctionnement en ajoutant plusieurs champs de dates sur la table des auteurs :

* `date_connexion` : la date de dernière connexion
* `date_connexion_precedente` : la date de pénultième connexion
* `date_suivi_activite` : prend la date de pénultième connexion, sauf si elle est déjà plus récente

Une action `maj_date_activite` permet de définir la date de suivi d’activité de l'auteur connecté.
L'argument attendu est une date au format SQL : `2017-12-10 12:20:17`